﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using TechTalk.SpecFlow;

namespace BehaviourTests
{
    [Binding]
    public sealed class Definition
    {
        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef
        private static Thread _process;


        /// <exception cref="ArgumentNullException">The <paramref name="start" /> parameter is null. </exception>
        /// <exception cref="OutOfMemoryException">There is not enough memory available to start this thread. </exception>
        [Given("The service has started")]
        public void StartService()
        {
            if (_process == null)
            {
                _process = new Thread(StartConsoleApplication);
                _process.Start();                
            }

        }

        [When("I navigate to the page")]
        public void OpenPage()
        {
            Driver.Instance.Navigate().GoToUrl("http://localhost:8080");
        }

        [Then("The web page opens")]
        public void ThenTheResultShouldBe()
        {
            var result =  Driver.Instance.FindElements(By.Id("Title")).Count;
            Assert.AreEqual(1, result);
        }

        [Then("Marilyns name appears")]
        public void ThenMarilynsNameAppears()
        {
            var result = Driver.Instance.FindElement(By.Id("Title")).Text;
            Assert.AreEqual("Marilyn Stone Basket", result);
        }

        [Then("There are ten table items")]
        public void ThenThereAreTenTableItems()
        {
            var result = Driver.Instance.FindElements(By.CssSelector("#itemlist tr")).Count;
            //plus 1 for header
            Assert.AreEqual(11, result);
        }

        private void StartConsoleApplication()
        {
            var solDirectory = new DirectoryInfo(Assembly.GetExecutingAssembly().Location).Parent.Parent.Parent.Parent.FullName;
            var filePath = Path.Combine(solDirectory,
                @"BasketSelfHost\bin\debug\BasketSelfHost.exe");

            var proc = new Process
            {
                StartInfo =
                {
                    FileName = filePath,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    WorkingDirectory = Environment.CurrentDirectory
                }
            };
            proc.Start();
        }
    }

    public static class Driver
    {
        public static IWebDriver Instance { get; set; }


        static Driver()
        {
            Instance = new InternetExplorerDriver();
            Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
        }
    }
}
