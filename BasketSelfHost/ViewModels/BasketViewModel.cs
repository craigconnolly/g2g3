﻿using System.Collections.Generic;
using System.Linq;
using Basket_DAL.DTOs;

namespace BasketSelfHost.ViewModels
{
    public class BasketViewModel
    {
        public List<BasketItemViewModel> Basket { get; set; }
        public decimal PostageCost { get; set; }
        public string Carrier { get; set; }
        public string Currency { get; set; }
        public string Name { get; set; }

        public decimal TotalCost
        {
            get { return Basket.Sum(f => f.Cost) + PostageCost; }
        }

        public int ItemCount => Basket.Count;

        public BasketViewModel(BasketModel model)
        {
            Name = model.User.FirstName + " " + model.User.LastName;
            PostageCost = model.Postage.Cost;
            Carrier = model.Postage.Carrier;
            Currency = GetHtmlForCurrency(model.User.Currency);
            Basket = new List<BasketItemViewModel>();
            foreach (BasketItem item in model.Basket)
            {
                Basket.Add(new BasketItemViewModel(item));
            }
        }

        private string GetHtmlForCurrency(string currency)
        {
            switch (currency)
            {
                default:
                    return "£";
            }
        }
    }

    public class BasketItemViewModel
    {
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }

        public BasketItemViewModel(BasketItem model)
        {
            ProductName = model.ProductName;
            Description = model.Description;
            Cost = model.Cost;
            Category = model.Category;
            Image = model.Image;
        }
    }
}
