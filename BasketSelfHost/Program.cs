﻿using System;
using Nancy.Hosting.Self;

namespace BasketSelfHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var url = new Uri("http://localhost:8080");
            var hostConfiguration = new HostConfiguration
            {
                UrlReservations = new UrlReservations() {CreateAutomatically = true}
            };
            using (var host = new NancyHost(hostConfiguration, url))
            {
                host.Start();
                Console.Out.WriteLine("Server running on {0}", url);
                Console.ReadLine();
            }
        }
    }
}
