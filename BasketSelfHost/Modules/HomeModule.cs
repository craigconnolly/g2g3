﻿using BasketSelfHost.ViewModels;
using Basket_DAL;
using Nancy;

namespace BasketSelfHost.Modules
{
    public class HomeModule : NancyModule
    {
        private readonly IBasketRepository _basket;

        public HomeModule()
        {
            _basket = new BasketRepository();
            Get["/"] = _ =>
            {
                var model = new BasketViewModel(_basket.GetBasket());
                return View["Home", model];
            };
        }
    }
}
