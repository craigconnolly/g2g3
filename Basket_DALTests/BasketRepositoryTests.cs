﻿using Basket_DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Basket_DALTests
{
    [TestClass()]
    public class BasketRepositoryTests
    {
        [TestMethod()]
        [TestCategory("Integration")]
        public void GetBasketTest()
        {
            var basket = new BasketRepository();
            var model = basket.GetBasket();
            Assert.AreEqual("Marilyn", model.User.FirstName);
        }
    }
}