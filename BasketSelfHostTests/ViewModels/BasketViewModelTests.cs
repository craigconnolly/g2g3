﻿using BasketSelfHost.ViewModels;
using Basket_DAL.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BasketSelfHostTests.ViewModels
{
    [TestClass()]
    public class BasketViewModelTests
    {
        private Basket_DAL.DTOs.BasketModel _model;

        [TestInitialize]
        public void Setup()
        {
            _model = new BasketModel
            {
                Basket = new[]
                {
                    new BasketItem()
                    {
                        Category = "Cat A",
                        Cost = 12.34M,
                        Image = "testimage",
                        Description = "test description",
                        ProductName = "product A"
                    },
                    new BasketItem()
                    {
                        Category = "Cat B",
                        Cost = 1.00M,
                        Image = "testimage",
                        Description = "test description",
                        ProductName = "product B"
                    }
                },
                Postage = new PostageModel
                {
                    Cost = 2.00M,
                    Carrier = "Royal Mail"
                },
                User = new UserModel
                {
                    FirstName = "Craig",
                    LastName = "Connolly",
                    Email = "connolly.craig@googlemail.com",
                    Currency = "GBP",
                    Location = "Sheffield"
                }
            };
        }

        [TestMethod()]
        public void TottalItemsTest()
        {
            var vm = new BasketViewModel(_model);
            Assert.AreEqual(2, vm.ItemCount);
        }

        [TestMethod()]
        public void FullNameTest()
        {
            var vm = new BasketViewModel(_model);
            Assert.AreEqual("Craig Connolly", vm.Name);
        }

        [TestMethod()]
        public void CurrencySymbolTest()
        {
            var vm = new BasketViewModel(_model);
            Assert.AreEqual("£", vm.Currency);
        }

        [TestMethod()]
        public void TotalCostTest()
        {
            var vm = new BasketViewModel(_model);
            Assert.AreEqual(15.34M, vm.TotalCost);
        }
    }
}