﻿namespace Basket_DAL.DTOs
{
    public class PostageModel
    {
        public decimal Cost { get; set; }
        public string Carrier { get; set; }
    }
}