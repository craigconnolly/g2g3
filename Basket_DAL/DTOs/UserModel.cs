namespace Basket_DAL.DTOs
{
    public class UserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Location { get; set; }
        public string Currency { get; set; }
    }
}