using System.Collections.Generic;

namespace Basket_DAL.DTOs
{
    public class BasketModel
    {
        public UserModel User { get; set; }
        public IEnumerable<BasketItem> Basket { get; set; }
        public PostageModel Postage { get; set; }
    }
}