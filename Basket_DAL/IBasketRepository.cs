﻿using System.Net;
using Basket_DAL.DTOs;
using Newtonsoft.Json;

namespace Basket_DAL
{
    public interface IBasketRepository
    {
        BasketModel GetBasket();
    }

    public class BasketRepository : IBasketRepository
    {
        public BasketModel GetBasket()
        {
            BasketModel model;
            using (var client = new WebClient())
            {
                var json = client.DownloadString("http://unattended-test.azurewebsites.net/basket.json");
                model = JsonConvert.DeserializeObject<BasketModel>(json);
            }
            return model;
        }
    }
}
