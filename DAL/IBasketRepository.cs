﻿using System.Net;
using System.ComponentModel.Design.Serialization;

namespace DAL
{
    public interface IBasketRepository
    {

    }

    class BasketRepository : IBasketRepository
    {
        public BasketModel GetBasket()
        {
            using (var client = new WebClient())
            {
                var json = client.DownloadString("http://example.com/json");
                var serializer = new DataContractJsonSerializer();
                var serializer = new JavaScriptSerializer();
                SomeModel model = serializer.Deserialize<SomeModel>(json);
                // TODO: do something with the model
            }
        }
    }
}
