﻿using System;
using Nancy;

public class HomeModule : NancyModule
{
    public HomeModule()
    {
        Get["/"] = _ => {
            var model = new { title = "Basket" };
            return View["home", model];
        };
    }
}
